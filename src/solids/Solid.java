package solids;

import transforms.Point3D;

import java.awt.*;
import java.util.List;

public interface Solid {

    List<Point3D> getVerticies(); //seznam bodů objektu
    List<Integer> getIndicies(TypeTopology typeTopology); //vrací indexy


    default int getColorByEdge(int index){
     return Color.BLACK.getRGB();
    }

    int getTriangleColor(int id);
}
