package solids;

/***
 *Výčet topologií
 */
public enum TypeTopology {

    LINES,
    TRIANGLES,
}
