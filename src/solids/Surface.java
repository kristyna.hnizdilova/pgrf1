package solids;

import transforms.Bicubic;
import transforms.Cubic;
import transforms.Point3D;

import java.awt.*;
/***
 *Modelová třída pro plát
 */
public class Surface extends SolidData {

    private Bicubic bc;

    public Surface() {
        Point3D[] rb = new Point3D[16];
        rb[0]=new Point3D(1,1,1);
        rb[1]=new Point3D(2,1,1);
        rb[2]=new Point3D(3,1,1);
        rb[3]=new Point3D(4,1,1);
        rb[4]=new Point3D(1,4,2);
        rb[5]=new Point3D(2,4,2);
        rb[6]=new Point3D(3,1,2);
        rb[7]=new Point3D(4,1,2);
        rb[8]=new Point3D(1,1,3);
        rb[9]=new Point3D(2,4,3);
        rb[10]=new Point3D(3,4,3);
        rb[11]=new Point3D(4,1,3);
        rb[12]=new Point3D(1,1,4);
        rb[13]=new Point3D(2,1,4);
        rb[14]=new Point3D(3,1,4);
        rb[15]=new Point3D(4,1,4);

        bc = new Bicubic(Cubic.BEZIER, rb,0);
    }

    @Override
    public int getTriangleColor(int id) {return Color.PINK.getRGB();    }
}
