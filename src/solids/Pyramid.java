package solids;

import transforms.Point3D;

import java.awt.*;
/***
 *Modelová třída pro jehlan
 */
public class Pyramid extends SolidData{

    public Pyramid(double size, double height){
        verticies.add(new Point3D(size/2,size/2,height));
        verticies.add(new Point3D(0, 0, 0));
        verticies.add(new Point3D(0, size, 0));
        verticies.add(new Point3D(size, 0, 0));
        verticies.add(new Point3D(size, size, 0));

        indicies.add(0); indicies.add(1);
        indicies.add(0); indicies.add(2);
        indicies.add(0); indicies.add(3);
        indicies.add(0); indicies.add(4);

        indicies.add(1); indicies.add(2);
        indicies.add(2); indicies.add(4);
        indicies.add(3); indicies.add(4);
        indicies.add(3); indicies.add(1);

        indiciesTriangle.add(0);indiciesTriangle.add(1);indiciesTriangle.add(2);
        indiciesTriangle.add(0);indiciesTriangle.add(2);indiciesTriangle.add(4);

        indiciesTriangle.add(0);indiciesTriangle.add(3);indiciesTriangle.add(4);
        indiciesTriangle.add(0);indiciesTriangle.add(1);indiciesTriangle.add(3);

        indiciesTriangle.add(1);indiciesTriangle.add(2);indiciesTriangle.add(4);
        indiciesTriangle.add(1);indiciesTriangle.add(4);indiciesTriangle.add(3);

    }

    //barva jehlanu
    public int getColorByEdge(int index) {
        return Color.magenta.getRGB();
    }

    @Override
    public int getTriangleColor(int id) {
        switch (id){
            case 0: return new Color(246,226,14).getRGB();
            case 1: return new Color(221,203,12).getRGB();
            case 2: return new Color(196,180,11).getRGB();
            case 3: return new Color(172,158,9).getRGB();
            case 4: return new Color(147,135,8).getRGB();
            case 5: return new Color(123,113,7).getRGB();
        }
        return Color.YELLOW.getRGB();
    }
}
