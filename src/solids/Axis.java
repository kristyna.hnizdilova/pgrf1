package solids;

import transforms.Point3D;

import java.awt.*;
/***
 *Modelová třída pro osy
 */
public class Axis extends SolidData {



    public Axis(){
        verticies.add(new Point3D(0, 0, 0));
        verticies.add(new Point3D(1, 0, 0));
        verticies.add(new Point3D(0, 1, 0));
        verticies.add(new Point3D(0, 0, 1));

        indicies.add(0); indicies.add(1);
        indicies.add(0); indicies.add(2);
        indicies.add(0); indicies.add(3);
    }


    //barva jednotlivých os
    @Override
    public int getColorByEdge(int index) {
        switch (index){
            case 0: return Color.RED.getRGB();
            case 1: return Color.GREEN.getRGB();
            case 2: return Color.BLUE.getRGB();
        }
        return super.getColorByEdge(index);
    }

    @Override
    public int getTriangleColor(int id) {
        return 0;
    }
}
