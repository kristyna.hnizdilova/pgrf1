package solids;

import transforms.Cubic;
import transforms.Point3D;

import java.util.ArrayList;
import java.util.List;

/***
 *Abstraktní třída pro seznam vrcholů, indexů a trojúhelníků - jednotlivé druhy objektů z ní dědí
 */
public abstract class SolidData implements Solid {
    protected List<Point3D> verticies;
    protected List<Integer> indicies;
    protected List<Integer> indiciesTriangle;


    public SolidData(){
        verticies = new ArrayList<>();
        indicies = new ArrayList<>();
        indiciesTriangle = new ArrayList<>();
    }

    @Override
    public List<Point3D> getVerticies() {
        return verticies;
    }

    @Override
    public List<Integer> getIndicies(TypeTopology typeTopology) {
        if (typeTopology == TypeTopology.LINES) {
            return indicies;
        } else {
            return indiciesTriangle;
        }

    }


}
