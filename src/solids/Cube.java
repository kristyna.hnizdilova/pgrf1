package solids;

import transforms.Point3D;

import java.awt.*;
/***
 *Modelová třída pro krychli
 */
public class Cube extends SolidData {


    public Cube(double size){
        verticies.add(new Point3D(0, 0, 0));     // 0.
        verticies.add(new Point3D(0, size, 0));    // 1.
        verticies.add(new Point3D(size, 0, 0));    //2
        verticies.add(new Point3D(size, size, 0));    //3
        verticies.add(new Point3D(0, 0, size));     //4
        verticies.add(new Point3D(0, size, size));    //5
        verticies.add(new Point3D(size, 0, size));    //6
        verticies.add(new Point3D(size, size, size));   //7


        indicies.add(0); indicies.add(1);
        indicies.add(1); indicies.add(3);
        indicies.add(2); indicies.add(3);
        indicies.add(2); indicies.add(0);

        indicies.add(4); indicies.add(5);
        indicies.add(5); indicies.add(7);
        indicies.add(6); indicies.add(7);
        indicies.add(6); indicies.add(4);

        indicies.add(0); indicies.add(4);
        indicies.add(1); indicies.add(5);
        indicies.add(2); indicies.add(6);
        indicies.add(3); indicies.add(7);

        indiciesTriangle.add(0); indiciesTriangle.add(1); indiciesTriangle.add(3); //spodek
        indiciesTriangle.add(0); indiciesTriangle.add(3); indiciesTriangle.add(2);

        indiciesTriangle.add(0); indiciesTriangle.add(1); indiciesTriangle.add(5); //předek
        indiciesTriangle.add(0); indiciesTriangle.add(5); indiciesTriangle.add(4);

        indiciesTriangle.add(1); indiciesTriangle.add(3); indiciesTriangle.add(7); //vpravo
        indiciesTriangle.add(1); indiciesTriangle.add(7); indiciesTriangle.add(5);

        indiciesTriangle.add(2); indiciesTriangle.add(3); indiciesTriangle.add(7); //vzadu
        indiciesTriangle.add(2); indiciesTriangle.add(7); indiciesTriangle.add(6);

        indiciesTriangle.add(0); indiciesTriangle.add(2); indiciesTriangle.add(6); //vlevo
        indiciesTriangle.add(0); indiciesTriangle.add(6); indiciesTriangle.add(4);

        indiciesTriangle.add(4); indiciesTriangle.add(5); indiciesTriangle.add(7); //vršek
        indiciesTriangle.add(4); indiciesTriangle.add(7); indiciesTriangle.add(6);



    }


    @Override
    public int getTriangleColor(int id) {
        switch (id){
            case 0: return new Color(128,112,220).getRGB();
            case 1: return new Color(115,100,198).getRGB();
            case 2: return new Color(102,89,176).getRGB();
            case 3: return new Color(89,78,154).getRGB();
            case 4: return new Color(76,67,132).getRGB();
            case 5: return new Color(64,56,110).getRGB();
            case 6: return new Color(51,44,88).getRGB();
            case 7: return new Color(38,33,66).getRGB();
            case 8: return new Color(128,112,220).getRGB();
            case 9: return new Color(115,100,198).getRGB();
            case 10: return new Color(102,89,176).getRGB();
            case 11: return new Color(89,78,154).getRGB();
            case 12: return new Color(76,67,132).getRGB();
            case 13: return new Color(64,56,110).getRGB();
            case 14: return new Color(51,44,88).getRGB();
            case 15: return new Color(38,33,66).getRGB();
        }
        return new Color(128,112,220).getRGB();

    }
}
