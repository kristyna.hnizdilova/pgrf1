package utils;

import solids.Axis;
import solids.Solid;
import solids.TypeTopology;
import transforms.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Optional;

public class Drawer {

    private BufferedImage img;

    private Mat4 model;
    private Mat4 view;
    private Mat4 projection;



    private Transformer transformer;


    public Drawer(BufferedImage img) {
        this.img = img;
        this.model = new Mat4Identity();
        this.view = new Mat4Identity();
        this.projection = new Mat4Identity();
        transformer = new Transformer(img);
    }

    //Vykreslení drátového modelu
    public void drawWireFrame(Solid solid) {
        // výsledná matice zobrazení
        Mat4 matFinal;
        if(solid instanceof Axis){
            matFinal = view.mul(projection);
        } else {
            matFinal = model.mul(view).mul(projection);
        }

        List<Integer> indices = solid.getIndicies(TypeTopology.LINES);
        for (int i = 0; i < indices.size(); i += 2) {
            Point3D a = solid.getVerticies().get(indices.get(i));
            Point3D b = solid.getVerticies().get(indices.get(i + 1));
            transformer.transformEdge(matFinal, a, b,
                    solid.getColorByEdge(i / 2));
        }


        // první index: 1. bod, druhý index: druhý bod úsečky
        for (int i = 0; i < solid.getIndicies(TypeTopology.LINES).size(); i += 2) {
            Point3D p1 = solid.getVerticies().get(solid.getIndicies(TypeTopology.LINES).get(i));
            Point3D p2 = solid.getVerticies().get(solid.getIndicies(TypeTopology.LINES).get(i + 1));
            transformer.transformEdge(matFinal, p1, p2, solid.getColorByEdge(i/2));
        }
    }

    public void drawFilledFrame(Solid solid) { // z-buffer
        Mat4 matFinal;
        if(solid instanceof Axis){
            matFinal = view.mul(projection);
            List<Integer> axes = solid.getIndicies(TypeTopology.LINES);
            for (int i = 0; i < axes.size(); i += 2) {
                Point3D a = solid.getVerticies().get(axes.get(i));
                Point3D b = solid.getVerticies().get(axes.get(i + 1));
                transformer.transformLine(a, b, matFinal,
                        solid.getColorByEdge(i / 2));
            }
        } else {
            matFinal = model.mul(view).mul(projection);
        }



        List<Integer> indices = solid.getIndicies(TypeTopology.TRIANGLES);
        for (int i = 0; i < indices.size() - 2; i += 3) {
            Point3D p1 = solid.getVerticies().get(indices.get(i));
            Point3D p2 = solid.getVerticies().get(indices.get(i + 1));
            Point3D p3 = solid.getVerticies().get(indices.get(i + 2));
            transformer.transformTriangles(p1, p2, p3, matFinal, solid.getTriangleColor(i/3));
        }
    }


    public Transformer getTransformer() {
        return transformer;
    }

    //nastavení model, view, projection

    public void setModel(Mat4 model) {
        this.model = model;
    }

    public void setView(Mat4 view) {
        this.view = view;
    }

    public void setProjection(Mat4 projection) {
        this.projection = projection;
    }
}