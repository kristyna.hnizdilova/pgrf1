package utils;

import transforms.Mat4;
import transforms.Point3D;
import transforms.Vec3D;

import java.awt.image.BufferedImage;
import java.util.Optional;

public class Transformer {

    private BufferedImage img;
    private RasterizerWireFrame rasterizerWireFrame;
    private RasterizerFilledFrame rasterizerFilledFrame;


    public Transformer(BufferedImage img){
        this.img = img;
        rasterizerWireFrame = new RasterizerWireFrame(img);
        rasterizerFilledFrame = new RasterizerFilledFrame(img);
    }

    public void transformTriangles(Point3D p1, Point3D p2, Point3D p3, Mat4 matFinal, int color) {

        p1 = p1.mul(matFinal);
        p2 = p2.mul(matFinal);
        p3 = p3.mul(matFinal);

        // řazení
        if (p1.getZ() < p2.getZ()) {
            Point3D p = p1;
            p1 = p2;
            p2 = p;
        }

        if (p2.getZ() < p3.getZ()) {
            Point3D p = p2;
            p2 = p3;
            p3 = p;
        }

        if (p1.getZ() < p2.getZ()) {
            Point3D p = p1;
            p1 = p2;
            p2 = p;
        }

        double zMin = 0;
        // 1. scénář (all in)
        if (p3.getZ() > zMin) {
            rasterizerFilledFrame.rasterizeTriangles(p1, p2, p3, color);
            return;
        }

        // 2. scénář (p3 out)
        if (p2.getZ() > zMin) {
            // t mezi p1 a p3
            double ta = ((zMin - p3.getZ()) / (p1.getZ() - p3.getZ()));
            Point3D p3a = p3.mul(1 - ta).add(p1.mul(ta));

            double tb = ((zMin - p3.getZ()) / (p2.getZ() - p3.getZ()));
            Point3D p3b = p3.mul(1 - tb).add(p2.mul(tb));

            rasterizerFilledFrame.rasterizeTriangles(p1, p2, p3a, color);
            rasterizerFilledFrame.rasterizeTriangles(p2, p3a, p3b, color);
            return;
        }
        // 3. scénář (only p1 in)
        if (p1.getZ() > zMin) {
            double t3 = ((zMin - p3.getZ()) / (p1.getZ() - p3.getZ()));
            double t2 = ((zMin - p2.getZ()) / (p1.getZ() - p2.getZ()));

            p3 = p3.mul(1 - t3).add(p1.mul(t3));
            p2 = p2.mul(1 - t2).add(p1.mul(t2));

            rasterizerFilledFrame.rasterizeTriangles(p1, p2, p3, color);
            return;
        }

        // 4. scénář (all out)
        return;

    }

    public void transformLine(Point3D p1, Point3D p2, Mat4 matFinal, int color) {

        p1 = p1.mul(matFinal);
        p2 = p2.mul(matFinal);

        // řazení
        if (p1.getZ() < p2.getZ()) {
            Point3D p = p1;
            p1 = p2;
            p2 = p;
        }



        double zMin = 0;

        //TODO ořez úsečky
        if (p1.getZ() > zMin) {
            rasterizerFilledFrame.rasterizeLine(p1, p2, color);
        }

    }

    //Pipeline
    public void transformEdge(Mat4 mat, Point3D p1, Point3D p2, int color) {


        //vynásobit body maticí
        p1 = p1.mul(mat);
        p2 = p2.mul(mat);


        //ořez dle W bodů

        Point3D t;
        if (p1.getW() > p2.getW()) {
            t = p2;
            p2 = new Point3D(p1);
            p1 = new Point3D(t);
        } else {
            p1 = new Point3D(p1);
            p2 = new Point3D(p2);
        }

        if (p2.getW() > 0.01) {
            if (p1.getW() < 0.01) {
                double te = (p1.getW() - 0.001) / (p1.getW() - p2.getW());
                p1 = p1.mul(te-1).add(p2.mul(-te));
            }
        }

        //tvorba z vektorů dehomogenizací

        Optional<Vec3D> vo1 = p1.dehomog();
        Optional<Vec3D> vo2 = p2.dehomog();

        if (!vo1.isPresent() || !vo2.isPresent())
            return;

        Vec3D v1 = vo1.get();
        Vec3D v2 = vo2.get();

        //ořez zobrazovacím objemem

        if (v1.getX() < -1 && v2.getX() < -1) return;
        if (v1.getX() > 1 && v2.getX() > 1) return;

        if (v1.getY() < -1 && v2.getY() < -1) return;
        if (v1.getY() > 1 && v2.getY() > 1) return;

        if (v1.getZ() < 0 && v2.getZ() < 0) return;
        if (v1.getZ() > 1 && v2.getZ() > 1) return;


        //přepočet souřadnic na výšku/šírku našeho okna (viewport)
        v1 = v1.mul(new Vec3D(1, 1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D(
                        0.5 * (img.getWidth() - 1),
                        0.5 * (img.getHeight() - 1),
                        1));

        v2 = v2.mul(new Vec3D(1, 1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D(
                        0.5 * (img.getWidth() - 1),
                        0.5 * (img.getHeight() - 1),
                        1));



        //ykreslení výsledku
        rasterizerWireFrame.lineDDA((int) v1.getX(), (int) v1.getY(), (int) v2.getX(),
                (int) v2.getY(), color);
    }


    public RasterizerFilledFrame getRasterizerFilledFrame() {
        return rasterizerFilledFrame;
    }
}
