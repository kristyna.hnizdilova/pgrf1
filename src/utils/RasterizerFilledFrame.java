package utils;

import transforms.Point3D;
import transforms.Vec3D;

import java.awt.image.BufferedImage;
import java.util.Optional;

public class RasterizerFilledFrame {


    private BufferedImage img;
    private double[][] zBuffer;

    public RasterizerFilledFrame(BufferedImage img){
        this.img = img;
        this.zBuffer = new double[img.getWidth()][img.getHeight()];
    }

    public void rasterizeTriangles(Point3D p1, Point3D p2, Point3D p3, int color) {

        // dehomogenizace
        Optional<Vec3D> vo1 = p1.dehomog();
        Optional<Vec3D> vo2 = p2.dehomog();
        Optional<Vec3D> vo3 = p3.dehomog();

        // řešení Optional<E>
        if (!vo1.isPresent() || !vo2.isPresent() || !vo3.isPresent()) return;

        Vec3D v1 = vo1.get();
        Vec3D v2 = vo2.get();
        Vec3D v3 = vo3.get();

        // ořez zobrazovacím objemem
        if (v1.getX() < -1 && v2.getX() < -1) return;
        if (v1.getX() > 1 && v2.getX() > 1) return;

        if (v1.getY() < -1 && v2.getY() < -1) return;
        if (v1.getY() > 1 && v2.getY() > 1) return;

        if (v1.getZ() < 0 && v2.getZ() < 0) return;
        if (v1.getZ() > 1 && v2.getZ() > 1) return;

        if ((Math.min(Math.min(v1.getX(), v2.getX()), v3.getX()) > 1.0f)
                || (Math.max(Math.max(v1.getX(), v2.getX()), v3.getX()) <= -1.0f))
            return ;

        // transformace do okna

        v1 = v1.mul(new Vec3D(1, 1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D(
                        0.5 * (img.getWidth() - 1),
                        0.5 * (img.getHeight() - 1),
                        1));

        v2 = v2.mul(new Vec3D(1, 1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D(
                        0.5 * (img.getWidth() - 1),
                        0.5 * (img.getHeight() - 1),
                        1));
        v3 = v3.mul(new Vec3D(1, 1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D(
                        0.5 * (img.getWidth() - 1),
                        0.5 * (img.getHeight() - 1),
                        1));


        scanLine(v1, v2, v3, color);

    }

    public void rasterizeLine(Point3D p1, Point3D p2, int color) {

        // dehomogenizace
        Optional<Vec3D> vo1 = p1.dehomog();
        Optional<Vec3D> vo2 = p2.dehomog();

        // řešení Optional<E>
        if (!vo1.isPresent() || !vo2.isPresent()) return;

        Vec3D v1 = vo1.get();
        Vec3D v2 = vo2.get();

        // ořez zobrazovacím objemem
        if (v1.getX() < -1 && v2.getX() < -1) return;
        if (v1.getX() > 1 && v2.getX() > 1) return;

        if (v1.getY() < -1 && v2.getY() < -1) return;
        if (v1.getY() > 1 && v2.getY() > 1) return;

        if (v1.getZ() < 0 && v2.getZ() < 0) return;
        if (v1.getZ() > 1 && v2.getZ() > 1) return;

        if ((Math.min(Math.min(v1.getX(), v2.getX()), v2.getX()) > 1.0f)
                || (Math.max(Math.max(v1.getX(), v2.getX()), v2.getX()) <= -1.0f))
            return ;

        // transformace do okna

        v1 = v1.mul(new Vec3D(1, 1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D(
                        0.5 * (img.getWidth() - 1),
                        0.5 * (img.getHeight() - 1),
                        1));

        v2 = v2.mul(new Vec3D(1, 1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D(
                        0.5 * (img.getWidth() - 1),
                        0.5 * (img.getHeight() - 1),
                        1));



        scanLine(v1, v2, color);

    }

    private void scanLine(Vec3D v1, Vec3D v2, int color) {
        //podmínka - trivialní algoritmus
        // seřazení dle Y
        if (v1.getY() > v2.getY()) {
            Vec3D v = v1;
            v1 = v2;
            v2 = v;
        }


        if(Math.abs(v1.getY() - v2.getY()) >= Math.abs(v1.getX()-v1.getX())) {
            for (int y = (int) v1.getY() + 1; y <= v2.getY(); y++) {
                double s1 = (y - v1.getY()) / (v2.getY() - v1.getY());
                double x = (v1.getX() * (1 - s1) + v2.getX() * s1);
                double z = (v1.getZ() * (1 - s1) + v2.getZ() * s1);

                if (x > 0
                        && y > 0
                        && x < img.getWidth() - 1
                        && y < img.getHeight() - 1
                        && z < zBuffer[(int)x][y]) {
                    drawPixel((int)x, y, color);
                    zBuffer[(int)x][y] = z;
                }
            }

        }

        if (v1.getX() > v2.getX()) {
                Vec3D v = v1;
                v1 = v2;
                v2 = v;
        }

        for (int x = (int) v1.getX() + 1; x <= v2.getX(); x++) {
            double s1 = (x - v1.getX()) / (v2.getX() - v1.getX());
            double y = (v1.getY() * (1 - s1) + v2.getY() * s1);
            double z = (v1.getZ() * (1 - s1) + v2.getZ() * s1);

            if (x > 0
                    && y > 0
                    && x < img.getWidth() - 1
                    && y < img.getHeight() - 1
                    && z < zBuffer[ x][(int)y]) {
                 drawPixel(x, (int) y, color);
                 zBuffer[x][(int) y] = z;
            }
        }

    }

    private void scanLine(Vec3D v1, Vec3D v2, Vec3D v3, int color) {
        // seřazení dle Y
        if (v1.getY() > v2.getY()) {
            Vec3D v = v1;
            v1 = v2;
            v2 = v;
        }
        if (v2.getY() > v3.getY()) {
            Vec3D v = v2;
            v2 = v3;
            v3 = v;
        }
        if (v1.getY() > v2.getY()) {
            Vec3D v = v1;
            v1 = v2;
            v2 = v;
        }

        // od V1 do V2
        for (int y = (int) v1.getY() + 1; y <= v2.getY(); y++) {
            double s1 = (y - v1.getY()) / (v2.getY() - v1.getY());
            double s2 = (y - v1.getY()) / (v3.getY() - v1.getY());

            double x1 = (v1.getX() * (1 - s1) + v2.getX() * s1);
            double x2 = (v1.getX() * (1 - s2) + v3.getX() * s2);

            double z1 = (v1.getZ() * (1 - s1) + v2.getZ() * s1);
            double z2 = (v1.getZ() * (1 - s2) + v3.getZ() * s2);

            // seřazení dle X
            if (x1 > x2) {
                double a = x1;
                x1 = x2;
                x2 = a;
                a = z1;
                z1 = z2;
                z2 = a;
            }

            for (int x = (int) x1; x <= x2; x++) {
                double t = (x - x1) / (x2 - x1);
                double z = (z1 * (1 - t) + z2 * t);

                if (x > 0
                        && y > 0
                        && x < img.getWidth() - 1
                        && y < img.getHeight() - 1
                        && z < zBuffer[x][y]) {
                    drawPixel(x, y, color);
                    zBuffer[x][y] = z;
                }
            }
        }

        //od bodu v2 do bodu V3
        for (int y = (int) v2.getY() + 1; y <= v3.getY(); y++) {
            double s1 = (y - v2.getY()) / (v3.getY() - v2.getY());
            double s2 = (y - v1.getY()) / (v3.getY() - v1.getY());

            double x1 = (v2.getX() * (1 - s1) + v3.getX() * s1);
            double x2 = (v1.getX() * (1 - s2) + v3.getX() * s2);

            double z1 = (v2.getZ() * (1 - s1) + v3.getZ() * s1);
            double z2 = (v1.getZ() * (1 - s2) + v3.getZ() * s2);

            // seřazení podle x
            if (x1 > x2) {
                double a = x1;
                x1 = x2;
                x2 = a;
                a = z1;
                z1 = z2;
                z2 = a;
            }

            for (int x = (int) x1; x <= x2; x++) {
                double t = (x - x1) / (x2 - x1);
                double z = (z1 * (1 - t) + z2 * t);

                if (x > 0
                        && y > 0
                        && x < img.getWidth() - 1
                        && y < img.getHeight() - 1
                        && z < zBuffer[x][y]) {
                    drawPixel(x, y, color);
                    zBuffer[x][y] = z;
                }
            }
        }
    }

    public void clear() {
        for(int x = 0; x < img.getWidth(); x++)
            for(int y = 0; y < img.getHeight(); y++)
                zBuffer[x][y] = 1.0;
    }

    private void drawPixel(int x, int y, int color) {
        if (x < 0 || x >= 1200) return;
        if (y < 0 || y >= 800) return;
        img.setRGB(x, y, color);
    }

}
