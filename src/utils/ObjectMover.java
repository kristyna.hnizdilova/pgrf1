package utils;

import solids.Solid;
import transforms.Mat4RotXYZ;
import transforms.Mat4Scale;
import transforms.Mat4Transl;
import transforms.Point3D;

import java.util.List;

public class ObjectMover {

    public ObjectMover(){
    }

    //rotate, move, scale metody
    public void moveX(List<Solid> solids, int indexOfSolid, double moveX){
        for (int i = 0; i < solids.get(indexOfSolid).getVerticies().size() ; i++) {
            Point3D point = solids.get(indexOfSolid).getVerticies().get(i);
            solids.get(indexOfSolid).getVerticies().set(i,point.mul(new Mat4Transl(moveX,0,0)));
        }
    }
    public void moveY(List<Solid> solids, int indexOfSolid,double moveY){
        for (int i = 0; i < solids.get(indexOfSolid).getVerticies().size() ; i++) {
            Point3D point = solids.get(indexOfSolid).getVerticies().get(i);
            solids.get(indexOfSolid).getVerticies().set(i,point.mul(new Mat4Transl(0,moveY,0)));
        }

    }

    public void moveZ(List<Solid> solids, int indexOfSolid,double moveZ){
        for (int i = 0; i < solids.get(indexOfSolid).getVerticies().size() ; i++) {
            Point3D point = solids.get(indexOfSolid).getVerticies().get(i);
            solids.get(indexOfSolid).getVerticies().set(i,point.mul(new Mat4Transl(0,0,moveZ)));
        }
    }

    public void rotate(List<Solid> solids, int indexOfSolid,double rotate){
        for (int i = 0; i < solids.get(indexOfSolid).getVerticies().size(); i++) {
            Point3D point = solids.get(indexOfSolid).getVerticies().get(i);
            solids.get(indexOfSolid).getVerticies().set(i,point.mul(new Mat4RotXYZ(rotate,rotate,rotate)));
        }
    }

    public void scale(List<Solid> solids, int indexOfSolid,double scale){
        for (int i = 0; i < solids.get(indexOfSolid).getVerticies().size(); i++) {
            Point3D point = solids.get(indexOfSolid).getVerticies().get(i);
            solids.get(indexOfSolid).getVerticies().set(i,point.mul(new Mat4Scale(scale)));
        }

    }

}
