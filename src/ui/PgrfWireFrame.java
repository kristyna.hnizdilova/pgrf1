package ui;

import solids.*;
import transforms.*;
import utils.Drawer;
import utils.ObjectMover;
import utils.RasterizerFilledFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Kristyna Hnizdilova
 * @version 2.0
 * @since 2018
 *
 */

public class PgrfWireFrame extends JFrame {

    private static int FPS = 1000 / 30;
    private static int width = 1200;
    private static int height = 800;

    private Drawer drawer;
    private ObjectMover objectMover;

    private Camera camera;
    private List<Solid> solids;
    private BufferedImage img;
    private boolean firstPersp = false;

    private int beginX, beginY;
    private double moveX,moveY,moveZ;
    private double rotate;
    private double scale;
    private int indexOfSolid = 0;

    private JPanel panel, panel1;
    private boolean mode = true;


    public static void main(String[] args) {
        PgrfWireFrame frame = new PgrfWireFrame();
        frame.init(width, height);
    }

    private void init(int width, int height) {
        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

        // nastavení frame
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(width, height);
        setTitle("Úloha 3 - Kristýna Hnízdilová");
        panel = new JPanel();
        panel1 = new JPanel();
        panel1.setLayout(new BoxLayout(panel1, BoxLayout.LINE_AXIS));
        panel1.setBackground(Color.white);
        add(panel);
        add(panel1, BorderLayout.SOUTH);

        //init components
        objectMover = new ObjectMover();
        solids = new ArrayList<>();
        drawer = new Drawer(img);
        drawer.setModel(new Mat4Identity());

        camera = new Camera(new Vec3D(9, 7, -2),
                -2.5, 0.3, 0.3, true);
        drawer.setProjection(new Mat4PerspRH(1, 1, 1, 100));


         //add solids
        initSolids();

        // listeners
        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                beginX = e.getX();
                beginY = e.getY();
                super.mousePressed(e);
            }
        });
        panel.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                camera = camera.addAzimuth((Math.PI / 1000) * (beginX - e.getX()));
                camera = camera.addZenith((Math.PI / 1000) * (beginY - e.getY()));
                beginX = e.getX();
                beginY = e.getY();
                super.mouseDragged(e);
            }
        });
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_UP:
                        camera = camera.forward(0.1);
                        break;
                    case KeyEvent.VK_DOWN:
                        camera = camera.backward(0.1);
                        break;
                    case KeyEvent.VK_LEFT:
                        camera = camera.left(0.1);
                        break;
                    case KeyEvent.VK_RIGHT:
                        camera = camera.right(0.1);
                        break;
                    case KeyEvent.VK_Y:
                        camera = camera.up(0.1);
                        break;
                    case KeyEvent.VK_X:
                        camera = camera.down(0.1);
                        break;
                    case KeyEvent.VK_A:
                        moveX = moveX - 0.1;
                        objectMover.moveX(solids,indexOfSolid,moveX);
                        break;
                    case KeyEvent.VK_W:
                        moveZ = moveZ - 0.1;
                        objectMover.moveZ(solids,indexOfSolid,moveZ);
                        break;
                    case KeyEvent.VK_S:
                        moveZ = moveZ + 0.1;
                        objectMover.moveZ(solids,indexOfSolid,moveZ);
                        break;
                    case KeyEvent.VK_D:
                        moveX = moveX + 0.1;
                        objectMover.moveX(solids,indexOfSolid,moveX);
                        break;
                    case KeyEvent.VK_Q:
                        moveY = moveY - 0.1;
                        objectMover.moveY(solids,indexOfSolid,moveY);
                        break;
                    case KeyEvent.VK_E:
                        moveY = moveY + 0.1;
                        objectMover.moveY(solids,indexOfSolid,moveY);
                        break;
                    case KeyEvent.VK_Z:
                        rotate = rotate + 0.01;
                        objectMover.rotate(solids,indexOfSolid,rotate);
                        break;
                    case KeyEvent.VK_T:
                        rotate = rotate - 0.01;
                        objectMover.rotate(solids,indexOfSolid,rotate);
                        break;
                    case KeyEvent.VK_P:
                        scale = scale +0.1;
                        objectMover.scale(solids,indexOfSolid,scale);
                        break;
                    case KeyEvent.VK_O:
                        scale = scale - 0.1;
                        objectMover.scale(solids,indexOfSolid,scale);
                        break;
                    case KeyEvent.VK_L:
                        if(firstPersp == true){
                            drawer.setProjection(new Mat4PerspRH(1, 1, 1, 100));
                            firstPersp=!firstPersp;
                        }else{
                            drawer.setProjection(new Mat4OrthoRH(15, 15, 0.1, 100));
                            firstPersp=!firstPersp;
                        }
                        break;

                    case KeyEvent.VK_NUMPAD1:
                        indexOfSolid = 0;
                        break;
                    case KeyEvent.VK_NUMPAD2:
                        indexOfSolid = 1;
                        break;
                    case KeyEvent.VK_NUMPAD3:
                        indexOfSolid = 2;
                        break;
                    case KeyEvent.VK_NUMPAD4:
                        indexOfSolid = 3;
                        break;
                    case KeyEvent.VK_NUMPAD5:
                        indexOfSolid = 4;
                        break;
                    case KeyEvent.VK_NUMPAD6:
                        indexOfSolid = 5;
                        break;
                    case KeyEvent.VK_R:
                        resetCamera();
                        break;
                    case KeyEvent.VK_M:
                        mode = !mode;
                        break;
                    case KeyEvent.VK_H:
                        help();
                        break;
                }

                super.keyReleased(e);
            }
        });

        help();
        setLocationRelativeTo(null);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                draw();
            }
        }, 100, FPS);

    }

    private void initSolids() {
        int count = 3;
        for (int i = 0; i < count; i++) {
            Cube cube = new Cube(1);
            for (int v = 0; v < cube.getVerticies().size(); v++) {
                Point3D point3D = cube.getVerticies().get(v);
                Point3D newPoint = point3D
                        .mul(new Mat4Transl(0, 2.8, 0))
                        .mul(new Mat4RotZ((double) i * 2d * Math.PI / (double) count));
                cube.getVerticies().set(v, newPoint);
            }
            solids.add(cube);
        }

        for (int i = 0; i < count; i++) {
            Pyramid pyramid = new Pyramid(1,2);
            for (int v = 0; v < pyramid.getVerticies().size(); v++) {
                Point3D point3D = pyramid.getVerticies().get(v);
                Point3D newPoint = point3D
                        .mul(new Mat4Transl(0.5, 3, 0.3))
                        .mul(new Mat4RotZ((double) i * 2d * Math.PI / (double) count));
                pyramid.getVerticies().set(v, newPoint);
            }
            solids.add(pyramid);
        }

        Axis axis = new Axis();
        for (int i = 0; i < axis.getVerticies().size(); i++) {
            Point3D point3D = axis.getVerticies().get(i);
            axis.getVerticies().set(i, point3D.mul(new Mat4Scale(6)));
        }
        solids.add(axis);
        //Surface s = new Surface();
        //solids.add(s);

    }

    private void help() {
        JOptionPane.showMessageDialog(panel, "Ovládání " +
                " \n CAMERA šipky a táhnutí myši, Y a X - pohyb nahoru a dolů, R - reset camery" +
                " \n VÝBĚR OBJEKTU - numpad 1 - 6" +
                " \n POHYB ZVOLENÉHO OBJEKTU - W,S,A,D,Q,E " +
                " \n ROTACE ZVOLENÉHO OBJEKTU - T, Z" +
                " \n PŘEPÍNÁNÍ PERSPEKTIVY - L)" +
                " \n PŘEPNUTÍ NA DRÁTOVÝ MODEL A ZPĚT - M");
    }

    private void resetCamera() {
        camera =  new Camera(new Vec3D(9, 7, -2),
                -2.5, 0.3, 0.3, true);

    }

    private void draw() {
        // clear
        img.getGraphics().fillRect(0, 0, img.getWidth(), img.getHeight());

        //clear zBuffer
        drawer.getTransformer().getRasterizerFilledFrame().clear();

        //nastavení modelu a pohledu
        drawer.setModel(new Mat4Identity());
        drawer.setView(camera.getViewMatrix());

        //výkres objektů
        for (Solid solid : solids) {
            if(mode == true)
            {
                drawer.drawFilledFrame(solid); // výplněné objekty
            }
            else
            {
                drawer.drawWireFrame(solid); // drátový model
            }
        }
        panel.getGraphics().drawImage(img, 0, 0, null);
        panel.paintComponents(getGraphics());
    }

}

